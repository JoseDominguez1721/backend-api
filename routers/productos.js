const { Router } = require("express");
const { arrayProductos } = require("../schemas/producto");
const product = Router();

product.get('/products', (req, res) => {
    arrayProductos.find().then(e => res.json(e)).catch(() => console.log("Algo salio mal"))
})

product.post('/post', (req, res) => {
    const body = req.body
    const peticion = enviandoDatos(body)
    peticion.then(() => res.status(401).send("Articulo creado y enviado")).catch(() => res.send("Algo salio mal"))
})

const enviandoDatos = async body => {
    const newProduct = new arrayProductos(body)
    return newProduct.save()
}

module.exports = { product }
