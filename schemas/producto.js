const { Schema, model } = require("mongoose");

const schemaProducto = new Schema({
    articulo: {
        type: String,
        required: true
    },
    marca: {
        type: String,
        required: true
    },
    imagen: {
        type: String,
        required: true
    },
    masVendido: {
        type: Boolean,
        required: true
    },
    envio: {
        type: Boolean,
        required: true
    },
    precio: {
        type: Number,
        required: true
    },
    descuento: {
        type: Number
    }
})

const arrayProductos = new model("productos", schemaProducto);

module.exports = { arrayProductos }