const express = require("express");
const cors = require("cors");
require('dotenv').config();
const PORT = process.env.PORT || 8033;
const app = express();
const { dbconexion } = require("./database");
const { product } = require("./routers/productos");

const main = () => {
    app.use(express.json())
    app.use(cors())
    app.use('/api', product)
    try{
        app.listen(PORT, () => {
            console.log(`Servidor funcionando en: ${PORT}`);
        })
    } catch (err){
        console.log(err);
    }
}

dbconexion()
    .then(main)
    .catch(err => {
        console.log(err)
    })